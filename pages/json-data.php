<?php
$servername = "localhost";
$username="root";
$password="1234";
$dbname ="parking";
// http://localhost/parking/json-data.php
try {
    $conn= new PDO("mysql:host=$servername;dbname=$dbname;charset=UTF8;",$username, $password );
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT PID, RID, region, c_name, image, imgtype, intro, 
    tel,address1,district, charge_1to4, charge_567,video, park_amount, el_park_no, 
    el_facility,Latitude, Longitude, el_image, el_imgtype FROM parking_ele ");
    $stmt ->execute();
    $stmt ->setFetchMode(PDO::FETCH_ASSOC);
    $JSONARR = array();

    foreach($stmt->fetchAll()as $row){
    $JSONOBJ = array( 
    "PID"=>$row["PID"], "RID"=>$row["RID"], "region"=>$row["region"], "c_name"=>$row["c_name"],
    "image"=>'data:'.$row["imgtype"].';base64,'. base64_encode($row["image"]), 
    "intro"=>$row["intro"], "tel"=>$row["tel"], "address1"=>$row["address1"], "district"=>$row["district"],
    "charge_1to4"=>$row["charge_1to4"], "charge_567"=>$row["charge_567"], "video"=>$row["video"], "park_amount"=>$row["park_amount"],
    "el_park_no"=>$row["el_park_no"], "el_facility"=>$row["el_facility"], "Latitude"=>$row["Latitude"], "Longitude"=>$row["Longitude"],
    "el_image"=>'data:'.$row["el_imgtype"].';base64,'. base64_encode($row["el_image"])
    );
    array_push($JSONARR,$JSONOBJ);
    }
     
    echo json_encode($JSONARR, JSON_UNESCAPED_UNICODE);
}catch (PDOException $e)
{
    echo "Error". $e->getMessage();
}
$conn =null;
?>
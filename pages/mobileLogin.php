<?php
header ("Access-Control-Allow-Orgin:*");
header ('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
header ('Access-Control-Allow-Headers: x-Requested-With, Content-Type, Accept');

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "ct247dsDB";

try{

    $mjson = file_get_contents("php://input");
    $result = json_decode($mjson);
   
    $conn = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // prepare sql and bind parameters
    $stmt = $conn->prepare("SELECT _id, email, fullname FROM memberINFO WHERE email=:email AND password=:password LIMIT 1");
        
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':password', $password);

    $email=$result->email;
    $password = md5($result->password);
    
    $stmt->execute();

    if($stmt->rowCount()>0){
              
    echo $JSON_RESULT = '{"code":"200", "result": "Login Successful"}';
   
    }else{
        echo $JSON_RESULT = '{"code":"404", "result": "Account Not Found"}';
    }
}catch(PDOException $e){
   
        $JSON_RESULT = '{"code":"404", "result": "Account Not Found"}';
        echo json_encode( $JSON_RESULT, JSON_UNESCAPED_UNICODE);
    
    }
   
?>
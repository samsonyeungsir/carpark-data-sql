-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 2019-12-25 15:29:47
-- 伺服器版本： 10.4.8-MariaDB
-- PHP 版本： 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `parking`
--

-- --------------------------------------------------------

--
-- 資料表結構 `carparkdetails`
--

CREATE TABLE `carparkdetails` (
  `_CID` int(11) NOT NULL,
  `Region` varchar(3) DEFAULT NULL,
  `SpaceAvailable` int(3) DEFAULT NULL,
  `District` varchar(20) DEFAULT NULL,
  `cName` varchar(20) DEFAULT NULL,
  `cAddress` varchar(50) DEFAULT NULL,
  `Latitude` float DEFAULT NULL,
  `Longitude` decimal(10,0) DEFAULT NULL,
  `Tel` char(9) DEFAULT NULL,
  `image` longblob DEFAULT NULL,
  `imgtype` text DEFAULT NULL,
  `website` varchar(30) DEFAULT NULL,
  `cDisable` char(0) DEFAULT NULL,
  `Motocycle` char(0) DEFAULT NULL,
  `Monthly` char(0) DEFAULT NULL,
  `Weekday` int(2) DEFAULT NULL,
  `WeekendPH` int(2) DEFAULT NULL,
  `Remarks` varchar(100) DEFAULT NULL,
  `GoogleLink` varchar(50) DEFAULT NULL,
  `Video` varchar(11) DEFAULT NULL,
  `regDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `carparkdetails`
--

INSERT INTO `carparkdetails` (`_CID`, `Region`, `SpaceAvailable`, `District`, `cName`, `cAddress`, `Latitude`, `Longitude`, `Tel`, `image`, `imgtype`, `website`, `cDisable`, `Motocycle`, `Monthly`, `Weekday`, `WeekendPH`, `Remarks`, `GoogleLink`, `Video`, `regDate`) VALUES
(3, 'KLN', 77, 'Kwun Tong', 'Kingston Internation', '19 Wang Chiu Road, Kowloon Bay, KLN', 22.3236, '114', '35895161', 0x696d6167652f4a706567, '', '', '', '', '', 0, 0, 'Height 2M', '', '', '0000-00-00 00:00:00'),
(4, 'KLN', 75, 'KLN - Cheung Sha Wan', 'Landmark East Carpar', '', 0, '0', '', '', '', '', '', '', '', 0, 0, '', '', '', '0000-00-00 00:00:00'),
(5, 'KLN', 73, 'San Po Kong', 'New Tech Plaza', '', 0, '0', '', '', '', '', '', '', '', 0, 0, '', '', '', '0000-00-00 00:00:00'),
(6, 'KLN', 59, 'Kowloon Bay', 'Kai Tak Cruise Termi', '', 0, '0', '', '', '', '', '', '', '', 0, 0, '', '', '', '0000-00-00 00:00:00'),
(7, 'KLN', 53, 'Kwun Tong', 'Paul Y Centre', '', 0, '0', '', '', '', '', '', '', '', 0, 0, '', '', '', '0000-00-00 00:00:00'),
(8, 'NT', 72, 'NT - Kwai Fong', 'Kwai Fong Car Park', '19 Kwai Yi Road, Kwai Chung, New Territories', 22.3555, '114', '2429 0566', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f74646370362e6a7067, '', 'https://www.wilsonparking.com.', '', '', '', 0, 0, 'Day Park  0700-1900 $100\nNight Park 1900-0700 $55', '', '', '0000-00-00 00:00:00'),
(9, 'NT', 0, 'NT - Tsuen Wan', 'TSUEN WAN CAR PARK', '174-208  Castle Peak Road, Tsuen Wan, New Terr', 22.3728, '114', '2611 9148', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f74646370322e6a7067, '', 'https://www.wilsonparking.com.', '', '', '', 0, 0, 'Day Park  0700-1900 $100\nNight Park 1900-0700 $60\nMonthly (non-reserved) $2100', '', '', '0000-00-00 00:00:00'),
(10, 'NT', 0, 'NT - Tuen Mun', 'Tuen Mun Government ', 'No. 1 Tuen Hi Road, Tuen Mun', 22.39, '114', '96828850', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f7464633136703534342e6a7067, '', 'https://www.wilsonparking.com.', '', '', '', 8, 22, 'Max Park (Sat/Sun/PH) $110', '', '', '0000-00-00 00:00:00'),
(11, 'NT', 0, 'NT - Tseung Kwan O', 'Po Lam Car Park', 'Po Lam Shopping Centre, 18 Po Lam Road North, Tseu', 22.3262, '114', '2830 3845', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f746463313470353133372e6a7067, '', 'http://www.linkhk.com/en/parki', '', '', '', 19, 22, '12 Hours Park\nMon-Fri (Excl.PH)  $100\nSat/Sun/PH $115', '', '', '0000-00-00 00:00:00'),
(12, 'NT', 0, 'NT - Tseung Kwan O', 'Ying Ming Court Car ', 'Ying Ming Court Car Park, 20 Po Lam Road North, Ts', 22.3258, '114', '2830 3845', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f746463313470353133362e6a7067, '', 'http://www.linkhk.com/en/parki', '', '', '', 19, 21, '12 Hours Park\nMon-Fri (Excl.PH)  $110\nSat/Sun/PH $125', '', '', '0000-00-00 00:00:00'),
(13, 'HKG', 0, 'HKG - Chai Wan', 'Wan Tsui Car Park', 'Wan Tsui Commercial Complex, 2 Wah \\nHa Street, Ch', 22.2619, '114', '2830 3845', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f746463313470313033332e6a7067, '', 'http://www.linkhk.com/en/parki', '', '', '', 19, 21, '12 Hours Park\nMon-Fri (Excl.PH)  $100\nSat/Sun/PH $115\n24 hours Park $125', '', '', '0000-00-00 00:00:00'),
(14, 'HKG', 0, 'HKG - Chai Wan', 'Tsui Wan Car Park', 'Tsui Wan Estate Retail and Car Park, 3 Tsui Wan St', 22.2673, '114', '2830 3845', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f746463313470313033322e6a7067, '', 'http://www.linkhk.com/en/parki', '', '', '', 21, 23, '12 Hours Park\nMon-Fri (Excl.PH)  $100\nSat/Sun/PH $110\n24 Hours Park Mon-Fri(excl. PH) $135\n24 hours ', '', '', '0000-00-00 00:00:00'),
(15, 'HKG', 0, 'HKG - Sai Ying Pun', 'Wah Ming Car Park', 'Wah Ming Shopping Centre, 21 Wah Ming Road, Fanlin', 22.4839, '114', '3471 2340', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f746463313470323131322e6a7067, '', 'http://www.linkhk.com/en/parki', '', '', '', 24, 24, 'Monthly (Non-reserved)  $4000\nMonthly (Reserved)   $4500', '', '', '0000-00-00 00:00:00'),
(16, 'HKG', 0, 'HKG - Shau Kei Wan', 'Hing Tung Car Park', 'Hing Tung Shopping Centre, 55 Yiu Hing Road, Shau ', 22.2805, '114', '2830 3845', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f746463313470313031372e6a7067, '', 'http://www.linkhk.com/en/parki', '', '', '', 21, 24, '12 Hours Park\nMon-Fri (Excl.PH)  $100\nSat/Sun/PH $110\n24 Hours Park Mon-Fri(excl. PH) $135\n24 hours ', '', '', '0000-00-00 00:00:00'),
(17, 'HKG', 0, 'HKG - Shau Kei Wan', 'Yiu Tung Car Park 2', 'Yiu Tung Shopping Centre, 12 Yiu Hing Road, Shau K', 22.2773, '114', '2830 3845', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f746463313470313033372e6a7067, '', 'http://www.linkhk.com/en/parki', '', '', '', 20, 22, '12 Hours Park\nMon-Fri (Excl.PH)  $195\nSat/Sun/PH $105\n24 hours Park $125', '', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- 資料表結構 `parking_ele`
--

CREATE TABLE `parking_ele` (
  `PID` int(11) NOT NULL,
  `RID` char(3) NOT NULL,
  `distritic` varchar(10) NOT NULL,
  `c_name` varchar(30) NOT NULL,
  `image` text NOT NULL,
  `intro` text NOT NULL,
  `tel` char(10) NOT NULL,
  `address` varchar(100) NOT NULL,
  `district` varchar(20) NOT NULL,
  `charge` text NOT NULL,
  `charge_1to4` varchar(100) NOT NULL,
  `charge_567` varchar(100) NOT NULL,
  `video` varchar(120) NOT NULL,
  `park_amount` varchar(100) NOT NULL,
  `el_park_no` int(11) NOT NULL,
  `el_facility` varchar(200) NOT NULL,
  `map` text NOT NULL,
  `Latitude` float DEFAULT NULL,
  `Longitude` decimal(10,0) DEFAULT NULL,
  `el_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `parking_ele`
--

INSERT INTO `parking_ele` (`PID`, `RID`, `distritic`, `c_name`, `image`, `intro`, `tel`, `address`, `district`, `charge`, `charge_1to4`, `charge_567`, `video`, `park_amount`, `el_park_no`, `el_facility`, `map`, `Latitude`, `Longitude`, `el_image`) VALUES
(1, 'HKI', '港島', '山頂廣場停車場', '', '山頂廣場是香港著名地標，於2017年起進行為期2年半的大型翻新工程，帶來全新景象。', '28494113', '香港山頂山頂道118號', '中西區', '星期一至四 05：00- 11：00 $26, 17：00-23：00 $38 ／2hr , 23:00- 05:00 $ 38, 五, 六、日及公眾假期 $38', '$26', '$38', 'https://www.youtube.com/watch?v=dc-seGMbqo0', '0', 1, '交流電快速充電 (21kW IEC Type 2和 13安培插座)', 'https://goo.gl/maps/6FBnfeCkg9LKrxni8', 22.2704, '114', ''),
(2, 'HKI', '港島', '天星碼頭停車場', '', '私家車/客貨車泊位數目:377;電單車泊位:37;4,400(非固定泊位)/5,900(固定泊位)', '21186917', '香港中環愛丁堡廣場9號', '中西區', '7:00 AM -19:00 $22 , 19:00-7:00am $16', '$22', '$16', 'https://www.youtube.com/watch?v=Oo3jQCt-BIg', '377', 4, '直流電快速充電 (50kW CHAdeMO 標準)&直流/交流電快速充電 (41kW IEC Type 2、50kW CCS DC Combo 及 50kW CHAdeMO 標準)', 'https://goo.gl/maps/dULARUF8fWnzoSCa7', 22.2826, '114', ''),
(3, 'HKI', '港島', '太古城中心停車場', '', '太古城中心是港島區規模最大的購物中心之一。', '25688665', '香港太古城太古城道18號', '東區', '$21', '$21', '$21', 'https://www.youtube.com/watch?v=HMVX1pNCkR4', '1019', 2, '交流電快速充電 (21kW IEC Type 2和 13安培插座)&直流/交流電快速充電 (41kW IEC Type 2、50kW CCS DC Combo 及 50kW CHAdeMO 標準)', 'https://goo.gl/maps/zGAUD4z6i7UacmBb6', 22.2867, '114', ''),
(4, 'HKI', '港島', '赤柱廣場停車場', '', '電動車免費泊車優惠」本優惠之成功登記者須使用已登記八達通進入指定停車場，並於早上10時至晚上9時30分於客戶服務台出示領展旗下參與電動車泊車優惠之商場／街市任何消費金額之即日機印發票以辦理最多3小時免費泊車優惠。', '28303845', '香港赤柱佳美道23號', '南區', '平日$25, 六、日及公眾假期 $36', '$25', '$36', 'https://www.youtube.com/watch?v=dII-66kEPrU', '228', 1, '交流電快速充電 (21kW IEC Type 2和 13安培插座)', 'https://g.page/stanleyplazahk?share', 22.2191, '114', ''),
(5, 'HKI', '港島', '數碼港1號停車場', '', '停車場 (一) 及 (二)：提供530 個私家車泊位；24 個電單車泊車位停車場 ', '31663800', '香港 數碼港道100號 數碼港2座', '南區', 'HK$22 (08:00 – 19:59) HK$8 (20:00 – 07:59) 入閘起每二十四小時最高收費$110.', '$22', '$22', 'https://www.youtube.com/watch?v=m7yq9Kg2FLg', '783', 2, '交流電快速充電 (21kW IEC Type 2和 13安培插座)', 'https://goo.gl/maps/esKtMKSzp1hnkrnG7', 22.2601, '114', ''),
(6, 'KWL', '九龍', 'MegaBox', '', 'MegaBox 連接多條主要幹線，除鄰近九龍灣港鐵站，無論駕車或乘搭公共交通工具，均可輕鬆抵達。 ', '29893000', '九龍九龍灣宏照道38號MegaBox 地庫停車場', '觀塘區', '平日$19, 六、日及公眾假期 $22', '$19', '$22', 'https://www.youtube.com/watch?v=wskXp0m3TuI', '', 3, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/6jH5Q32uShpjaLhJ6', 22.3199, '114', ''),
(7, 'KWL', '九龍', '油麗商場', '', '毗鄰港鐵油塘站，油麗商場是房屋署「東九龍太古城」的一部分，連接油塘站上架空購物廊及油塘邨第四期「大本型」。', '23530667', '油塘油塘道9號油麗商場停車場LG1層', '觀塘區', '$17', '$17', '$17', 'https://www.youtube.com/watch?v=hr-TVQDt5SY', '', 7, '多制式快速充電 (CCS, CHAdeMO & IEC Type 2 AC 3 Phase)&雙制式中速充電（歐式IEC標準及英式標準）', 'https://goo.gl/maps/4RfMzvMq3aFTDBJc8', 22.3199, '114', ''),
(8, 'KWL', '九龍', '樂富中心', '', '樂富廣場停車場車位數目共793個。樂富廣場為首個領展開設官方獨立網站的旗下商場。 ', '27883070', '黃大仙橫頭磡樂富中心停車場地下', '黃大仙', '平日$19, 六、日及公眾假期 $21', '$19', '$21', 'https://www.youtube.com/watch?v=HAL232gfe0U', '793', 3, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/RryveUPdK7R4D6AB6', 22.3386, '114', ''),
(9, 'KWL', '九龍', '美麗華商場', '', '美麗華廣場位於九龍尖沙咀，樓高8層，佔地54萬平方呎。上蓋的美麗華廣場A座是樓高18層的寫字樓。', '23155868', '尖沙咀彌敦道132號美麗華商場停車場B2層', '油尖旺區', '平日$39, 五, 六、日及公眾假期 $41, ', '$39', '$41', 'https://www.youtube.com/watch?v=dtr-6rB6u0U', '', 3, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/RxeDRceYWBVLGR4GA', 22.3009, '114', ''),
(10, 'KWL', '九龍', '奧海城二期', '', '奧海城為香港九龍大角咀以及旺角西西九龍填海區內的建築群，屬於港鐵奧運站物業發展項目。', '27404108', '西九龍海庭道18號奧海城二期停車場地下', '油尖旺區', '平日$19, 六、日及公眾假期 $28', '$19', '$28', 'https://www.youtube.com/watch?v=hU8oBr7j2XU', '', 2, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/BYYJANDyVnEARdrf9', 22.3172, '114', ''),
(11, 'NTT', '新界', '上水廣場', '', '上水廣場為北區的大型商場，是區內最大型的購物商場，而地面層則爲新的上水巴士總站所在地。 ', '26399638', '上水龍琛路39號上水廣場停車場B3層', '北區', '平日$20, 六、日及公眾假期 $23', '$20', '$23', 'https://www.youtube.com/watch?v=acSpV3eYBv8', '', 3, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/VSDG4q1iocfJmWhQ7', 22.5026, '114', ''),
(12, 'NTT', '新界', '黃金海岸商場', '', '黃金海岸商場位於青山灣畔，歐陸式的建築充滿地中海浪漫風情。', '24526566', '屯門青山公路1號黃金海岸商場停車場地庫', '屯門區', '平日$11, 五, 六、日及公眾假期 $14', '$11', '$14', 'https://www.youtube.com/watch?v=acSpV3eYBv9', '', 2, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/fvLDXWmPWJZohyb88', 22.3719, '113', ''),
(13, 'NTT', '新界', '沙田中心', '', '沙田中心位於新界沙田區的商住物業，為沙田市中心發展範圍內最早落成的屋苑,居民和顧客前往港鐵站及沙田市中心各處可以全程不用日曬雨淋，也免於出現人車爭路的情況。 ', '26063700', '沙田橫壆街2-16號沙田中心停車場二樓', '沙田區', '平日$19, 五, 六、日及公眾假期 $25', '$19', '$25', 'https://www.youtube.com/watch?v=acSpV3eYBv10', '', 3, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/MLYoUeaqE7dF9BPR7', 22.3826, '114', ''),
(14, 'NTT', '新界', '長發商場', '', '長發廣場是一個位於新界青衣長發邨一帶的購物中心。是領展其中一個重點發展商場。', '28303845', '青衣長發邨長發商場停車場二樓', '葵青區', '平日$17, 六、日及公眾假期 $23', '$17', '$23', 'https://www.youtube.com/watch?v=DcjeWIMW1RQ', '', 3, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/fDVBvhDw7PJGNCP96', 22.3622, '114', ''),
(15, 'NTT', '新界', '東薈城 ', '', '東薈城位於大嶼山東涌的一座商場。 ', '21092933', '東涌達東路20號東薈城停車場北面地庫一樓', '離島區', '北面：$14／hr  南面 ： $16／hr', '$16', '$16', 'https://www.youtube.com/watch?v=fvZwVQP27Zg', '', 2, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/TV14VnvD4fAbmnEQ7', 22.2894, '113', '');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `carparkdetails`
--
ALTER TABLE `carparkdetails`
  ADD PRIMARY KEY (`_CID`);

--
-- 資料表索引 `parking_ele`
--
ALTER TABLE `parking_ele`
  ADD PRIMARY KEY (`PID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

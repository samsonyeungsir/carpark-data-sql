<?php
$servername = "localhost";
$username="root";
$password="1234";
$dbname ="ct247dsDB";
// http://localhost/ctphp/json-data.php
try {
    $conn= new PDO("mysql:host=$servername;dbname=$dbname;charset=UTF8;",$username, $password );
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT _id, firstname, lastname, stuimg, imgtype FROM stuDetails");
    $stmt ->execute();
    $stmt ->setFetchMode(PDO::FETCH_ASSOC);
    $JSONARR = array();

    foreach($stmt->fetchAll()as $row){
    $JSONOBJ = array( "_id"=>$row["_id"], "firstname"=>$row["firstname"], "lastname"=>$row["lastname"],
    "stuimg"=>'data:'.$row["imgtype"].';base64,'. base64_encode($row["stuimg"]));
    array_push($JSONARR,$JSONOBJ);
    }
     
    echo json_encode($JSONARR, JSON_UNESCAPED_UNICODE);
}catch (PDOException $e)
{
    echo "Error". $e->getMessage();
}
$conn =null;
?>
<?PHP
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "ct247dsDB";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT _id, firstname, lastname, email FROM stuDetails"); 
    $stmt->execute();

    $JSONresult = $stmt->fetchAll();
    echo json_encode($JSONresult);
      
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
?>
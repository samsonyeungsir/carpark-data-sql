-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- 主機： localhost
-- 產生時間： 2019 年 12 月 19 日 05:23
-- 伺服器版本： 10.4.6-MariaDB
-- PHP 版本： 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `parking`
--

-- --------------------------------------------------------

--
-- 資料表結構 `parking_com`
--

CREATE TABLE `parking_com` (
  `PID` int(11) NOT NULL,
  `RID` char(3) NOT NULL,
  `distritic` varchar(10) NOT NULL,
  `c_name` varchar(30) NOT NULL,
  `image` text NOT NULL,
  `intro` text NOT NULL,
  `tel` char(10) NOT NULL,
  `address` varchar(100) NOT NULL,
  `charge` text NOT NULL,
  `charge_1to4` varchar(100) NOT NULL,
  `charge_567` varchar(100) NOT NULL,
  `video` varchar(120) NOT NULL,
  `space_availablet` varchar(100) NOT NULL,
  `el_park_no` int(11) NOT NULL,
  `el_facility` varchar(200) NOT NULL,
  `map` text NOT NULL,
  `el_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `parking_ele`
--

CREATE TABLE `parking_ele` (
  `PID` int(11) NOT NULL,
  `RID` char(3) NOT NULL,
  `distritic` varchar(10) NOT NULL,
  `c_name` varchar(30) NOT NULL,
  `image` text NOT NULL,
  `intro` text NOT NULL,
  `tel` char(10) NOT NULL,
  `address` varchar(100) NOT NULL,
  `district` varchar(20) NOT NULL,
  `charge` text NOT NULL,
  `charge_1to4` varchar(100) NOT NULL,
  `charge_567` varchar(100) NOT NULL,
  `video` varchar(120) NOT NULL,
  `park_amount` varchar(100) NOT NULL,
  `el_park_no` int(11) NOT NULL,
  `el_facility` varchar(200) NOT NULL,
  `map` text NOT NULL,
  `Latitude` float DEFAULT NULL,
  `Longitude` decimal(10,0) DEFAULT NULL,
  `el_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `parking_ele`
--

INSERT INTO `parking_ele` (`PID`, `RID`, `distritic`, `c_name`, `image`, `intro`, `tel`, `address`, `district`, `charge`, `charge_1to4`, `charge_567`, `video`, `park_amount`, `el_park_no`, `el_facility`, `map`, `el_image`) VALUES
(1, 'HKI', '港島', '山頂廣場停車場', '', '山頂廣場是香港著名地標，於2017年起進行為期2年半的大型翻新工程，帶來全新景象。翻新後最為曯目的是其如閃耀寶石般設計的弧形入口，加上明亮通透的全玻璃幕牆設計充滿時代感。', '28494113', '香港山頂山頂道118號', '中西區', '星期一至四 05：00- 11：00 $26, 17：00-23：00 $38 ／2hr , 23:00- 05:00 $ 38, 五, 六、日及公眾假期 $38', '', '', 'https://www.youtube.com/watch?v=dc-seGMbqo0', '0', 1, '交流電快速充電 (21kW IEC Type 2和 13安培插座)', 'https://goo.gl/maps/6FBnfeCkg9LKrxni8', ''),
(2, 'HKI', '港島', '天星碼頭停車場', '', '私家車/客貨車泊位數目:377;電單車泊位:37;4,400(非固定泊位)/5,900(固定泊位)', '21186917', '香港中環愛丁堡廣場9號', '中西區', '7:00 AM -19:00 $22 , 19:00-7:00am $16', '', '', 'https://www.youtube.com/watch?v=Oo3jQCt-BIg', '377', 4, '直流電快速充電 (50kW CHAdeMO 標準)&直流/交流電快速充電 (41kW IEC Type 2、50kW CCS DC Combo 及 50kW CHAdeMO 標準)', 'https://goo.gl/maps/dULARUF8fWnzoSCa7', ''),
(3, 'HKI', '港島', '太古城中心停車場', '', '太古城中心（Cityplaza），位於香港島東區鰂魚涌太古城，是港島區規模最大的購物中心之一，後來與太古城一樣，成為港島東的其中一部分。太古城中心共有四期，設有辦公大廈及商場。太古城中心一期早在1982年落成，但曾在1993年至2000年期間進行重建及翻新工程。第二期商場亦在1987年落成，面向維多利亞港的第三及四期辦公大廈（已出售）則在1992年至1993年落成。', '25688665', '香港太古城太古城道18號', '東區', '$21', '', '', 'https://www.youtube.com/watch?v=HMVX1pNCkR4', '1019', 2, '交流電快速充電 (21kW IEC Type 2和 13安培插座)&直流/交流電快速充電 (41kW IEC Type 2、50kW CCS DC Combo 及 50kW CHAdeMO 標準)', 'https://goo.gl/maps/zGAUD4z6i7UacmBb6', ''),
(4, 'HKI', '港島', '赤柱廣場停車場', '', '電動車免費泊車優惠」本優惠之成功登記者須使用已登記八達通進入指定停車場，並於早上10時至晚上9時30分於客戶服務台出示領展旗下參與電動車泊車優惠之商場／街市任何消費金額之即日機印發票以辦理最多3小時免費泊車優惠。', '28303845', '香港赤柱佳美道23號', '南區', '平日$25, 六、日及公眾假期 $36', '', '', 'https://www.youtube.com/watch?v=dII-66kEPrU', '228', 1, '交流電快速充電 (21kW IEC Type 2和 13安培插座)', 'https://g.page/stanleyplazahk?share', ''),
(5, 'HKI', '港島', '數碼港1號停車場', '', '數碼港自2004年成立以來，致力推動本地創新科技發展，並主力推動六大科技範疇，包括金融科技、智慧生活、數碼娛樂及電子競技、大數據與人工智慧、區塊鏈及網絡安全；亦擔當培育年輕人、初創及企業家投身數碼科技行業的角色，並積極為他們聯繫策略夥伴和投資者，促進科技企業於本地及國際商業夥伴的合作，以及推動大型企業和中小企數碼轉型。[停車場 (一) 及 (二)：提供530 個私家車泊位；24 個電單車泊車位停車場 (三)：提供253個私家車泊位；18 個電單車泊車位', '', '香港 數碼港道100號 數碼港2座', '南區', 'HK$22 (08:00 – 19:59) HK$8 (20:00 – 07:59) 入閘起每二十四小時最高收費$110.', '', '', 'https://www.youtube.com/watch?v=m7yq9Kg2FLg', '783', 2, '交流電快速充電 (21kW IEC Type 2和 13安培插座)', 'https://goo.gl/maps/esKtMKSzp1hnkrnG7', ''),
(6, 'KWL', '九龍', 'MegaBox', '', 'MegaBox 連接多條主要幹線，除鄰近九龍灣港鐵站，無論駕車或乘搭公共交通工具，均可輕鬆抵達。 ', '29893000', '九龍九龍灣宏照道38號MegaBox 地庫停車場', '觀塘區', '平日$19, 六、日及公眾假期 $22', '', '', 'https://www.youtube.com/watch?v=wskXp0m3TuI', '', 3, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/6jH5Q32uShpjaLhJ6', ''),
(7, 'KWL', '九龍', '油麗商場', '', '毗鄰港鐵油塘站，主要服務油麗邨、油塘邨和油翠苑的居民，於2010年開放使用。油麗商場是房屋署「東九龍太古城」的一部分，連接油塘站上架空購物廊及油塘邨第四期「大本型」。', '23530667', '油塘油塘道9號油麗商場停車場LG1層', '觀塘區', '$17', '', '', 'https://www.youtube.com/watch?v=hr-TVQDt5SY', '', 7, '多制式快速充電 (CCS, CHAdeMO & IEC Type 2 AC 3 Phase)&雙制式中速充電（歐式IEC標準及英式標準）', 'https://goo.gl/maps/4RfMzvMq3aFTDBJc8', ''),
(8, 'KWL', '九龍', '樂富中心', '', '樂富廣場（英語：Lok Fu Place）位於香港九龍黃大仙區樂富聯合道198號，共分二期，總共有389,616 平方呎零售面積，停車場車位數目共793個[1]，是領展目前最具規模的「重點商場」。商場前稱為樂富中心，於2009年1月15日更名。領展管理於2008年起為物業進行耗資超過4.23億港元的提升工程，於2011年初全部完成。[2]領展於2016年將英文名稱由Lok Fu Plaza改為Lok Fu Place。樂富廣場為首個領展開設官方獨立網站的旗下商場。 ', '27883070', '黃大仙橫頭磡樂富中心停車場地下', '黃大仙', '平日$19, 六、日及公眾假期 $21', '', '', 'https://www.youtube.com/watch?v=HAL232gfe0U', '793', 3, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/RryveUPdK7R4D6AB6', ''),
(9, 'KWL', '九龍', '美麗華商場', '', '美麗華廣場（英文：Mira Place）前稱：栢麗廣場，位於香港九龍尖沙咀彌敦道118-130及132號，樓高8層，佔地54萬平方呎。上蓋的美麗華廣場A座是樓高18層的寫字樓，每層佔地39,152平方呎。項目由恆基兆業發展，於1996年落成，商場部份範圍於2008年及2014年至2016年曾進行翻新。2017年6月由美麗華商場改名為美麗華廣場，英文名亦同時變更。 ', '23155868', '尖沙咀彌敦道132號美麗華商場停車場B2層', '油尖旺區', '平日$39, 五, 六、日及公眾假期 $41, ', '', '', 'https://www.youtube.com/watch?v=dtr-6rB6u0U', '', 3, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/RxeDRceYWBVLGR4GA', ''),
(10, 'KWL', '九龍', '奧海城二期', '', '奧海城（英語：Olympian City）為香港九龍大角咀以及旺角西西九龍填海區內的建築群，屬於港鐵奧運站物業發展項目，是九龍區主要商場及住宅之一，一期及二期商場及住宅部份（即維港灣、柏景灣、帝柏海灣）由信和置業及港鐵公司發展，現時由信和集團持有並由信和管業優勢管理，而三期商場及住宅部分（即帝峯 · 皇殿）則由信和置業發展與管理。2012年，商場人流及營業額分別錄得25%及17%，升幅創新高，全年推廣費為4,000萬港元。現時奧海城平均平日人流為32萬人次。此外，2012年全年商場的營業額18億元，較2011年增長17%。', '27404108', '西九龍海庭道18號奧海城二期停車場地下', '油尖旺區', '平日$19, 六、日及公眾假期 $28', '', '', 'https://www.youtube.com/watch?v=hU8oBr7j2XU', '', 2, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/BYYJANDyVnEARdrf9', ''),
(11, 'NTT', '新界', '上水廣場', '', '上水廣場（Landmark North）為香港北區的大型商場，位於港鐵上水站對面。商場樓高5層，其中2樓至5樓為購物樓層，設99間商舖；6樓至22樓為辦公大樓。發展商為新鴻基地產，於1995年正式開幕。由於上水廣場頂部外型設計獨特，也是區內最大型的購物商場，而地面層則爲新的上水巴士總站所在地。 ', '26399638', '上水龍琛路39號上水廣場停車場B3層', '北區', '平日$20, 六、日及公眾假期 $23', '', '', 'https://www.youtube.com/watch?v=acSpV3eYBv8', '', 3, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/VSDG4q1iocfJmWhQ7', ''),
(12, 'NTT', '新界', '黃金海岸商場', '', '黃金海岸商場（英文：Gold Coast Piazza）位於青山灣畔，歐陸式的建築充滿地中海浪漫風情。早年英文名稱為Marina Magic Shopping Mall。商場提供多姿多采的娛樂，設計富地中海情調，有幽雅的平台花園，飽覽遊艇會景致，設有多家餐廳食肆及Market Place by Jasons超級市場，還有充裕的地方可供休憩和感受歐陸情懷。 ', '24526566', '屯門青山公路1號黃金海岸商場停車場地庫', '屯門區', '平日$11, 五, 六、日及公眾假期 $14', '', '', 'https://www.youtube.com/watch?v=acSpV3eYBv9', '', 2, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/fvLDXWmPWJZohyb88', ''),
(13, 'NTT', '新界', '沙田中心', '', '沙田中心（英語：Shatin Centre）是一座位於香港新界沙田區的商住物業，由恆基兆業發展興建，並由旗下的恆益物業管理公司作物業管理，此項目由李景勳建築師負責設計，1977年開始設計，於1981年12月落成入伙，為沙田市中心發展範圍內最早落成的屋苑，共有八座樓宇。商場部份主要設於L3樓（L1樓亦有少量商店），並與整個沙田市中心的行人天橋網絡及港鐵沙田站作無縫連接，居民和顧客前往港鐵站及沙田市中心各處可以全程不用日曬雨淋，也免於出現人車爭路的情況，而L1和L2樓則為停車場。沙田中心商場於1997年9月至11月，以至2015年11月起進行大型翻新工程。 ', '26063700', '沙田橫壆街2-16號沙田中心停車場二樓', '沙田區', '平日$19, 五, 六、日及公眾假期 $25', '', '', 'https://www.youtube.com/watch?v=acSpV3eYBv10', '', 3, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/MLYoUeaqE7dF9BPR7', ''),
(14, 'NTT', '新界', '長發商場', '', '長發廣場（英語：Cheung Fat Plaza），舊稱長發商場（英語：Cheung Fat Shopping Centre），是一個位於香港新界青衣長發邨一帶的購物中心。廣場於1989年落成，位於青衣北部，是青衣北面居民的主要購物地方。廣場原為房屋署購物中心，現為領展（港交所：0823）旗下的購物中心，是領展其中一個重點發展商場。領展在2007年開始為物業提升資產，進行不同程度的翻新工程。截至2010年7月，廣場已翻新完成，並且被改稱為廣場。 ', '28303845', '青衣長發邨長發商場停車場二樓', '葵青區', '平日$17, 六、日及公眾假期 $23', '', '', 'https://www.youtube.com/watch?v=DcjeWIMW1RQ', '', 3, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/fDVBvhDw7PJGNCP96', ''),
(15, 'NTT', '新界', '東薈城 ', '', '東薈城（Citygate）位於香港大嶼山東涌的一座商場，鄰近港鐵東涌站，香港國際機場和昂坪360，商場由五層高購物商場，九層高的商業大廈和酒店三部分組成，由香港五間發展商：恆隆地產、恆基地產、新世界發展、新鴻基地產及太古地產各佔20%股權的Newfoundworld Investment Holdings Limited聯合發展，並由太古地產管理。商場部分於2000年4月8日正式啟用，酒店部分命名為諾富特東薈城酒店，於2006年初開業。東薈城進行擴建和局部重建於2019年7月峻工，並於同年8月20日起分階段開幕，預計於同年9月全面開幕。 ', '21092933', '東涌達東路20號東薈城停車場北面地庫一樓', '離島區', '北面：$14／hr  南面 ： $16／hr', '', '', 'https://www.youtube.com/watch?v=fvZwVQP27Zg', '', 2, '雙制式中速充電 （歐式IEC標準及英式標準）', 'https://goo.gl/maps/TV14VnvD4fAbmnEQ7', '');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `parking_com`
--
ALTER TABLE `parking_com`
  ADD PRIMARY KEY (`PID`);

--
-- 資料表索引 `parking_ele`
--
ALTER TABLE `parking_ele`
  ADD PRIMARY KEY (`PID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2019 at 12:51 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mycarparkdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `carparkdetails`
--

CREATE TABLE `carparkdetails` (
  `_CID` int(11) NOT NULL,
  `Region` varchar(3) DEFAULT NULL,
  `SpaceAvailable` int(3) DEFAULT NULL,
  `District` varchar(20) DEFAULT NULL,
  `cName` varchar(20) DEFAULT NULL,
  `cAddress` varchar(50) DEFAULT NULL,
  `Latitude` float DEFAULT NULL,
  `Longitude` decimal(10,0) DEFAULT NULL,
  `Tel` char(9) DEFAULT NULL,
  `image` longblob DEFAULT NULL,
  `imgtype` text DEFAULT NULL,
  `website` varchar(30) DEFAULT NULL,
  `cDisable` char(0) DEFAULT NULL,
  `Motocycle` char(0) DEFAULT NULL,
  `Monthly` char(0) DEFAULT NULL,
  `Weekday` int(2) DEFAULT NULL,
  `WeekendPH` int(2) DEFAULT NULL,
  `Remarks` varchar(100) DEFAULT NULL,
  `GoogleLink` varchar(50) DEFAULT NULL,
  `Video` varchar(11) DEFAULT NULL,
  `regDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `carparkdetails`
--

INSERT INTO `carparkdetails` (`_CID`, `Region`, `SpaceAvailable`, `District`, `cName`, `cAddress`, `Latitude`, `Longitude`, `Tel`, `image`, `imgtype`, `website`, `cDisable`, `Motocycle`, `Monthly`, `Weekday`, `WeekendPH`, `Remarks`, `GoogleLink`, `Video`, `regDate`) VALUES
(3, 'KLN', 77, 'Kwun Tong', 'Kingston Internation', '19 Wang Chiu Road, Kowloon Bay, KLN', 22.3236, '114', '35895161', 0x696d6167652f4a706567, '', '', '', '', '', 0, 0, 'Height 2M', '', '', '0000-00-00 00:00:00'),
(4, 'KLN', 75, 'KLN - Cheung Sha Wan', 'Landmark East Carpar', '', 0, '0', '', '', '', '', '', '', '', 0, 0, '', '', '', '0000-00-00 00:00:00'),
(5, 'KLN', 73, 'San Po Kong', 'New Tech Plaza', '', 0, '0', '', '', '', '', '', '', '', 0, 0, '', '', '', '0000-00-00 00:00:00'),
(6, 'KLN', 59, 'Kowloon Bay', 'Kai Tak Cruise Termi', '', 0, '0', '', '', '', '', '', '', '', 0, 0, '', '', '', '0000-00-00 00:00:00'),
(7, 'KLN', 53, 'Kwun Tong', 'Paul Y Centre', '', 0, '0', '', '', '', '', '', '', '', 0, 0, '', '', '', '0000-00-00 00:00:00'),
(8, 'NT', 72, 'NT - Kwai Fong', 'Kwai Fong Car Park', '19 Kwai Yi Road, Kwai Chung, New Territories', 22.3555, '114', '2429 0566', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f74646370362e6a7067, '', 'https://www.wilsonparking.com.', '', '', '', 0, 0, 'Day Park  0700-1900 $100\nNight Park 1900-0700 $55', '', '', '0000-00-00 00:00:00'),
(9, 'NT', 0, 'NT - Tsuen Wan', 'TSUEN WAN CAR PARK', '174-208  Castle Peak Road, Tsuen Wan, New Terr', 22.3728, '114', '2611 9148', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f74646370322e6a7067, '', 'https://www.wilsonparking.com.', '', '', '', 0, 0, 'Day Park  0700-1900 $100\nNight Park 1900-0700 $60\nMonthly (non-reserved) $2100', '', '', '0000-00-00 00:00:00'),
(10, 'NT', 0, 'NT - Tuen Mun', 'Tuen Mun Government ', 'No. 1 Tuen Hi Road, Tuen Mun', 22.39, '114', '96828850', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f7464633136703534342e6a7067, '', 'https://www.wilsonparking.com.', '', '', '', 8, 22, 'Max Park (Sat/Sun/PH) $110', '', '', '0000-00-00 00:00:00'),
(11, 'NT', 0, 'NT - Tseung Kwan O', 'Po Lam Car Park', 'Po Lam Shopping Centre, 18 Po Lam Road North, Tseu', 22.3262, '114', '2830 3845', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f746463313470353133372e6a7067, '', 'http://www.linkhk.com/en/parki', '', '', '', 19, 22, '12 Hours Park\nMon-Fri (Excl.PH)  $100\nSat/Sun/PH $115', '', '', '0000-00-00 00:00:00'),
(12, 'NT', 0, 'NT - Tseung Kwan O', 'Ying Ming Court Car ', 'Ying Ming Court Car Park, 20 Po Lam Road North, Ts', 22.3258, '114', '2830 3845', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f746463313470353133362e6a7067, '', 'http://www.linkhk.com/en/parki', '', '', '', 19, 21, '12 Hours Park\nMon-Fri (Excl.PH)  $110\nSat/Sun/PH $125', '', '', '0000-00-00 00:00:00'),
(13, 'HKG', 0, 'HKG - Chai Wan', 'Wan Tsui Car Park', 'Wan Tsui Commercial Complex, 2 Wah \\nHa Street, Ch', 22.2619, '114', '2830 3845', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f746463313470313033332e6a7067, '', 'http://www.linkhk.com/en/parki', '', '', '', 19, 21, '12 Hours Park\nMon-Fri (Excl.PH)  $100\nSat/Sun/PH $115\n24 hours Park $125', '', '', '0000-00-00 00:00:00'),
(14, 'HKG', 0, 'HKG - Chai Wan', 'Tsui Wan Car Park', 'Tsui Wan Estate Retail and Car Park, 3 Tsui Wan St', 22.2673, '114', '2830 3845', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f746463313470313033322e6a7067, '', 'http://www.linkhk.com/en/parki', '', '', '', 21, 23, '12 Hours Park\nMon-Fri (Excl.PH)  $100\nSat/Sun/PH $110\n24 Hours Park Mon-Fri(excl. PH) $135\n24 hours ', '', '', '0000-00-00 00:00:00'),
(15, 'HKG', 0, 'HKG - Sai Ying Pun', 'Wah Ming Car Park', 'Wah Ming Shopping Centre, 21 Wah Ming Road, Fanlin', 22.4839, '114', '3471 2340', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f746463313470323131322e6a7067, '', 'http://www.linkhk.com/en/parki', '', '', '', 24, 24, 'Monthly (Non-reserved)  $4000\nMonthly (Reserved)   $4500', '', '', '0000-00-00 00:00:00'),
(16, 'HKG', 0, 'HKG - Shau Kei Wan', 'Hing Tung Car Park', 'Hing Tung Shopping Centre, 55 Yiu Hing Road, Shau ', 22.2805, '114', '2830 3845', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f746463313470313031372e6a7067, '', 'http://www.linkhk.com/en/parki', '', '', '', 21, 24, '12 Hours Park\nMon-Fri (Excl.PH)  $100\nSat/Sun/PH $110\n24 Hours Park Mon-Fri(excl. PH) $135\n24 hours ', '', '', '0000-00-00 00:00:00'),
(17, 'HKG', 0, 'HKG - Shau Kei Wan', 'Yiu Tung Car Park 2', 'Yiu Tung Shopping Centre, 12 Yiu Hing Road, Shau K', 22.2773, '114', '2830 3845', 0x687474703a2f2f7265736f757263652e646174612e6f6e652e676f762e686b2f74642f6361727061726b2f746463313470313033372e6a7067, '', 'http://www.linkhk.com/en/parki', '', '', '', 20, 22, '12 Hours Park\nMon-Fri (Excl.PH)  $195\nSat/Sun/PH $105\n24 hours Park $125', '', '', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carparkdetails`
--
ALTER TABLE `carparkdetails`
  ADD PRIMARY KEY (`_CID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carparkdetails`
--
ALTER TABLE `carparkdetails`
  MODIFY `_CID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
